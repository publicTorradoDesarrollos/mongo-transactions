'use strict'

const Model1 = require('./model1')
const db = require('./connect')

async function create(){

  try{
    //START SESSION  
    const session = await db.getConnection().startSession();
    
    //START TRANSACTION
    session.startTransaction();
    
    //SAVE (NOT WORK)
    //var data = Model1.Model1({"field1":"prueba2"}) 
    //await data.save()
    
    //CREATE (OK)
    await Model1.Model1.create([{"field1":"RIver carajo!"}],{session:session})
    
    //COMMIT TRANSACTION
    await session.commitTransaction();

    //ABORT TRANSACTION
    //await session.abortTransaction();
    
    //END SESSION
    session.endSession();

  }catch(error){
      console.log(error)
  }
}


async function update(){

  try{
    //START SESSION  
    const session = await db.getConnection().startSession();
    
    //START TRANSACTION
    session.startTransaction();

    //UPDATE (OK)
    await Model1.Model1.findOneAndUpdate({"field1":"Pablo2"},{"$set":{"field1":"Pablo3"}},{new:true,session: session})
    
    //COMMIT TRANSACTION
    await session.commitTransaction();

    //ABORT TRANSACTION
    //await session.abortTransaction();
    
    //END SESSION
    session.endSession();

  }catch(error){
      console.log(error)
  }
}



module.exports={
    create,
    update
}