'use strict'

const mongoose = require('mongoose');

var options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

var url = "mongodb://api2:api2@localhost:27017/api2?retryWrites=false"
//"mongodb+srv://developer:g7Rib1xiNHRsYWXD@cluster0.odemj.mongodb.net/saquiApp?retryWrites=true&w=majority"

var db


function connectMongo(){
    mongoose.connect(url,options)
    .then(dbConection=> {
        db=dbConection
        console.log("Se conecto")
    }).catch((error=>console.log(error)))
}

function getConnection(){
    return db
}

module.exports={
    connectMongo,
    getConnection
}
